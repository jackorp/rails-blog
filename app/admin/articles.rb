ActiveAdmin.register Article do
  permit_params :title, :body, :category_id
end
