class Category < ApplicationRecord
  has_many :articles, dependent: :destroy
  validates :title, presence: true

  def to_param
    [id, title.parameterize].join("-")
  end
end
