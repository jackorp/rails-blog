Rails.application.routes.draw do
  # ActiveAdmin routing
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  get 'welcome/index'

  resources :categories, only: [:index, :show] do
    resources :articles, only: [:index, :show]
  end

  resources :articles, only: :show

  root 'welcome#index'
end
