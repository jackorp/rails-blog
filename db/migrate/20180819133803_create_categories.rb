class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :title

      t.timestamps
    end
    
    create_table :articles do |t|
      t.string :title, null: false
      t.text :body
      t.belongs_to :category, index: true

      t.timestamps
    end
  end
end
