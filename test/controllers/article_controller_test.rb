require 'test_helper'

class ArticleControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article = articles(:rubyfoo)
  end

  test "should show article" do
    get article_url(@article)
    assert_response :success
  end
end
