require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  def setup
    @article = articles(:valid)
  end

  test 'valid article' do
    assert @article.valid?
  end

  test 'invalid without title' do
    @article.title = nil
    refute @article.valid?, 'article saved without title'
    refute_empty @article.errors[:title], 'no validation error for title present'
  end

  test 'invalid without body' do
    @article.body = nil
    refute @article.valid?, 'article saved without body'
    refute_empty @article.errors[:body], 'no validation error for body present'
  end

  test '#category' do
    assert_respond_to @article, :category
  end
end
