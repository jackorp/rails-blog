require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  def setup
    @category = categories(:rubyonrails)
  end

  test 'valid category' do
    assert @category.valid?
  end

  test 'invalid without title' do
    @category.title = nil
    refute @category.valid?, 'Category saved without title'
    refute_empty @category.errors[:title], 'No validation error for title present'
  end

  test '#articles' do
    assert_equal 2, @category.articles.size
  end
end
